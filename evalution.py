  import sys
def fits(target,modern):
  i = 0
  j = 0
  while(i < len(target)):
    while(j < len(modern) and target[i] != modern[j]):
     j += 1
     if(j == len(modern)):
       return False
    i += 1
    j += 1
  return True


def solve(fossils):
  path1 = []
  path2 = []
  path3 = []
  total = [path1, path2, path3]
  for i in range(1, MAX):
    if(fossils[i][0] == 0):
      continue
    else:
      if(len(path3) == 0):
        if(fossils[i][1] == 0):
          if(len(path1) == 0):
            path1.append(fossils[i][0])
          elif(len(path2) == 0):
            if(fits(path1[-1], fossils[i][0])):
              path1.append(fossils[i][0])
            else:
              path2.append(fossils[i][0])
          else:
            x = fits(path1[-1], fossils[i][0])
            y = fits(path2[-1], fossils[i][0])
            if(x == False):
              path2.append(fossils[i][0])
            elif(y == False):
              path1.append(fossils[i][0])
            else:
              return "impossible"
        else:
          if(len(path1) == 0):
            path1.append(fossils[i][0])
            path2.append(fossils[i][1])
          elif(len(path2) == 0):
            if(fits(path1[-1], fossils[i][0])):
              path1.append(fossils[i][0])
              path2.append(fossils[i][1])
            elif(fits(path1[-1], fossils[i][1])):
              path1.append(fossils[i][1])
              path2.append(fossils[i][0])
            else:
              return "impossible"
          elif(fits(path1[-1], fossils[i][0]) and fits(path2[-1], fossils[i][1])):
            path1.append(fossils[i][0])
            path2.append(fossils[i][1])
          elif(fits(path1[-1], fossils[i][1]) and fits(path2[-1], fossils[i][0])):
            path1.append(fossils[i][1])
            path2.append(fossils[i][0])
          else:
            return "impossible"
  print(len(path1), len(path2))
  for i in path1:
    print(i)
  for j in path2:
    print(j)
          

      

MAX = 4001
n = int(sys.argv[1])
modern = sys.argv[2]
fossils = [[0, 0] for i in range(0,MAX)]
result = True
for i in range(0,n):
  sample = input()
  s = len(sample)
  if(fits(sample, modern) == False):
    result = False
    break
  elif(fossils[s][0] == 0):
    fossils[s][0] = sample
  elif(fossils[s][1] == 0):
    fossils[s][1] = sample
  else:
    result = False
if(result == False):
  print("impossible")
else:
  solve(fossils)
